export default function without (...blacklists) {
  const difference = new Set(this)
  for (const blacklist of blacklists) {
    for (const value of blacklist) {
      difference.delete(value)
    }
  }
  return difference
}
