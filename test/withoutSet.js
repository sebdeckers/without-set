import test from 'ava'
import without from '..'

test('remove blacklisted values', (t) => {
  const old = new Set(['foo.txt', 'bar.txt', 'baz.txt'])
  const now = new Set(['lol.txt', 'bar.txt'])

  const removed = old::without(now) // -> new Set(['foo.txt', 'baz.txt'])
  const added = now::without(old)   // -> new Set(['lol.txt'])

  t.deepEqual(Array.from(removed), ['foo.txt', 'baz.txt'])
  t.deepEqual(Array.from(added), ['lol.txt'])
})

test('multiple blacklists', (t) => {
  const given = new Set('sebastiaan')
  const blacklists = [
    new Set('a'),
    new Set('n')
  ]
  const actual = given::without(...blacklists)
  t.deepEqual(Array.from(actual), 'sebti'.split(''))
})
