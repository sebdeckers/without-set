# without-set

Returns the [complement](https://en.wikipedia.org/wiki/Complement_(set_theory)) of a set and one or more other sets.

## API

### `operand::without(...blacklist)`

Returns a `Set` containing elements that exist in `operand` but not in `blacklist`.

Designed for use with `::` function bind syntax, as the `this` property should be the Set to filter.

#### operand

`Set` instance to filter.

#### blacklist

One or more `Set` instances containing values to leave out of the result.

## Use Case

```js
import without from 'without-set'

const old = new Set(['foo.txt', 'bar.txt', 'baz.txt'])
const now = new Set(['lol.txt', 'bar.txt'])

const removed = old::without(now) // -> Set(['foo.txt', 'baz.txt'])
const added = now::without(old)   // -> Set(['lol.txt'])
```

## Peer Dependencies

This package uses Babel to transpile its code to be compatible with the node version it is installed on. Ensure you have installed the necessary peer dependencies.

```
npm install babel-cli babel-preset-env babel-plugin-transform-function-bind
```

On some older Node environments, e.g. 4.x and earlier, you may also require system-wide polyfills.

```
npm install babel-polyfill
```

And then load the `babel-polyfill` library before loading this package.

## See Also

- [wu](https://www.npmjs.com/package/wu)

## Colophon

Made by Sebastiaan Deckers in Singapore 🇸🇬
